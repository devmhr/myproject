<?php

namespace App\Http\Controllers;

use App\recipe;
use Illuminate\Http\Request;
use DB;

class RecipeController extends Controller
{
    //

    public function showIndex()
    {
        try {
            $result = recipe::all();
//            print_r($result); die();

            if (count($result) == 0) {
                return view('recipe.index')->with(['$message' => 'Database is empty']);
            } else {

                return view('recipe.index')->with(['recipes' => $result]);
            }
        } catch
        (\Exception $e) {
            return $e;
        }


    }

    public function showRecipeForm()
    {
        return view('recipe.create');
    }

    public function insertRecipe(Request $req)
    {
        try {
            $recipeobj = new recipe();
            $recipeobj->category = $req->input('category');
            $recipeobj->ingredient = $req->input('ingredient');
            $recipeobj->recipe = $req->input('recipe');
            $recipeobj->healthy = $req->input('healthy');

//            print_r($recipeobj);
//            die();
            $recipeobj->save();

            if ($recipeobj->save()) {
                return redirect()->route ('listrecipe')->with(['$message' => 'Your data has been added successfully']);
            }


        } catch (\Exception $e) {
            return $e;
        }
    }


    public function getSingleRecipe($id)
    {
        try {
                    $result=recipe::find($id);
//                    print_r($result); die();
            if (count($result) == 0) {
                return view('recipe.edit')->with(['$message' => 'Database is empty']);
            } else {

                return view('recipe.edit')->with(['recipe' => $result]);
            }
        } catch
        (\Exception $e) {
            return $e;
        }

    }

    public function  updateRecipe(Request $req)
            {
                try {
                    $id=$req->get( 'id');
                    $recipeobj = recipe::find($id);
                    $recipeobj->category = $req->input('category');
                    $recipeobj->ingredient = $req->input('ingredient');
                    $recipeobj->recipe = $req->input('recipe');
                    $recipeobj->healthy = $req->input('healthy');

//            print_r($recipeobj);
//            die();
                    $recipeobj->save();

                    if ($recipeobj->save()) {
                        return redirect()->route ('listrecipe')->with(['$message' => 'Your data has been updated successfully']);
                    }


                } catch (\Exception $e) {
                    return $e;
                }
    }
public function deleteRecipe($id)
{
            try{
                    DB::table('recipe')->where('id','=',$id)->delete();
                return redirect()->route ('listrecipe')->with(['$message' => 'Your data has been deleted successfully']);


            }
            catch(\Exception $e)
            {
                print_r($e);
            }
}

}
