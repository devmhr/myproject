<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/recipe',['uses'=>'RecipeController@showIndex','as'=>'listrecipe']);

Route::get('/recipe/create',['uses'=>'RecipeController@showRecipeForm','as'=>'addrecipe']);
Route::get('/recipe/edit/{id}',['uses'=>'RecipeController@getSingleRecipe','as'=>'editrecipe']);
Route::get('/recipe/delete/{id}',['uses'=>'RecipeController@deleteRecipe','as'=>'deleterecipe']);

Route::post('/recipe/create',['uses'=>'RecipeController@insertRecipe','as'=>'insertrecipe']);

Route::post('/recipe/update',['uses'=>'RecipeController@updateRecipe','as'=>'updaterecipe']);
