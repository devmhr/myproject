@extends('layouts.main')

@section('content')

    <div class="container">

        @if($recipe)
        <form action="{{route('updaterecipe')}}" method="POST">

            <div class="form-group">
                <label for="Category">Category</label>
                <input type="text" value="{{$recipe->category}}" name="category" class="form-control">
            </div>


            <div class="form-group">
                <label for="Ingredient">Ingredient required</label>
                <input type="text" value="{{$recipe->ingredient}}" name="ingredient" class="form-control">
            </div>


            <div class="form-group">
                <label for="recipe">Recipe Name</label>
                <input type="text" value="{{$recipe->recipe}}" name="recipe" class="form-control">
            </div>


            <div class="form-group">
                <label for="Healthy?">Healthy or not??</label>
                <input type="text" value="{{$recipe->healthy}}" name="healthy" class="form-control">
            </div>

            <input type="submit" value="Update" name="submit" class="btn btn-primary">

            <input type="hidden" name="_token" value="{{csrf_token()}}">

            <input type="hidden" name="id" value="{{$recipe->id}}">

        </form>

@endif

    </div>


    {{--table define garne--}}


@endsection