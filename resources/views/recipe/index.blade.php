@extends('layouts.main')

@section('content')

<div class="container">

    @if(isset($message))
        <div class="alert alert-success alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            {{$message}}
        </div>
        @endif

    <a href="{{route('addrecipe')}}" class="btn btn-primary">Add new recipe</a>
<hr>
    <table class="table table-responsive">

        <thead>
        <tr>
        <th>Id</th>
        <th>Category</th>
        <th>Ingredient</th>
        <th>Recipe</th>
        <th>Healthy?</th>
        <th>Actiomnm</th>

      </thead>
        </tr>
        <tbody>

        @if (isset($recipes))
           @foreach($recipes as $recipe)
        <tr>
            <td>{{$recipe->id}}</td>
            <td>{{$recipe->category}}</td>
            <td>{{$recipe->ingredient}}</td>
            <td>{{$recipe->recipe}}</td>
            <td>{{$recipe->healthy}}</td>
            <td>
                <a href="{{route('editrecipe',['id'=>$recipe->id])}}" class="btn btn-primary">Edit</a>
                <a href="{{route('deleterecipe',['id'=>$recipe->id])}}" class="btn btn-primary">Delete</a>
            </td>
        </tr>
            @endforeach
            @endif
        </tbody>
    </table>

</div>


    {{--table define garne--}}


    @endsection